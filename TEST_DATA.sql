-- Connect to the numitu database
\connect numitu

-- Example Authors
INSERT INTO authors(name, role) VALUES ('Annie Abernathy','Author');
INSERT INTO authors(name, role) VALUES ('Baily Beratheon','Baker');
INSERT INTO authors(name, role) VALUES ('Camilla Carusoe','Cartographer');
INSERT INTO authors(name, role) VALUES ('Damien DeMordue','Dentist');

-- Some basic tutorials test data
-- Some starting tutorial plans
INSERT INTO tutorials(tutorial_plan_id) VALUES (null);
INSERT INTO tutorials(tutorial_plan_id) VALUES (null);
INSERT INTO tutorials(tutorial_plan_id) VALUES (null);

-- Some tutorial sessons
INSERT INTO tutorials(tutorial_plan_id) VALUES (1);
INSERT INTO tutorials(tutorial_plan_id) VALUES (1);
INSERT INTO tutorials(tutorial_plan_id) VALUES (2);
INSERT INTO tutorials(tutorial_plan_id) VALUES (2);
INSERT INTO tutorials(tutorial_plan_id) VALUES (3);

-- Add a few activities to the tutorial plans
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (1, null);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (1, null);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (1, null);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (2, null);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (2, null);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (3, null);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (3, null);

-- Add a few activity sessions
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (4, 1);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (4, 2);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (5, 4);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (5, 6);
INSERT INTO activities(tutorial_id, activity_plan_id) VALUES (8, 7);

-- insert some issue and evidence activities
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (1);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (1);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (1);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (1);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (1);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (2);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (2);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (2);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (3);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (3);
INSERT INTO issue_and_evidence_activities(activity_id) VALUES (4);

-- insert some topics
INSERT INTO topics(issues_and_evidences_activity_id, name) VALUES (1, 'heart');
INSERT INTO topics(issues_and_evidences_activity_id, name) VALUES (1, 'brain');
INSERT INTO topics(issues_and_evidences_activity_id, name) VALUES (1, 'lungs');
INSERT INTO topics(issues_and_evidences_activity_id, name) VALUES (2, 'heart');
INSERT INTO topics(issues_and_evidences_activity_id, name) VALUES (2, 'eyes');
INSERT INTO topics(issues_and_evidences_activity_id, name) VALUES (3, 'stomach');

-- insert some issues
INSERT INTO issues(topic_id, data) VALUES (1, 'cardiac arrest');
INSERT INTO issues(topic_id, data) VALUES (2, 'brain dead');
INSERT INTO issues(topic_id, data) VALUES (2, 'internal bleeding');
INSERT INTO issues(topic_id, data) VALUES (3, 'asthma');
INSERT INTO issues(topic_id, data) VALUES (4, 'cardiac arrest');
INSERT INTO issues(topic_id, data) VALUES (5, 'cataracts');
INSERT INTO issues(topic_id, data) VALUES (5, 'glaucoma');

-- insert some evidence
INSERT INTO evidences(data, issue_id, score) VALUES ('arythmic heartbeat', 1, 10);
INSERT INTO evidences(data, issue_id, score) VALUES ('no response to stimuli', 2, 5);
INSERT INTO evidences(data, issue_id, score) VALUES ('cold', 2, 10);
INSERT INTO evidences(data, issue_id, score) VALUES ('difficulty breathing', 4, 15);
INSERT INTO evidences(data, issue_id, score) VALUES ('blind', 6, 10);