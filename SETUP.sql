DROP DATABASE IF EXISTS numitu;

CREATE DATABASE numitu
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;
GRANT ALL ON DATABASE numitu TO postgres;
REVOKE ALL ON DATABASE numitu FROM public;

\connect numitu

CREATE TABLE authors
(
  id bigserial NOT NULL,
  name text,
  role text,
  CONSTRAINT authors_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE authors
  OWNER TO postgres;
COMMENT ON TABLE authors
IS 'A table to store the authors of the NuMiTu project.';

CREATE TABLE tutorials
(
  id bigserial NOT NULL,
  tutorial_plan_id bigint references tutorials(id),
  CONSTRAINT tutorials_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tutorials
  OWNER TO postgres;
ALTER TABLE tutorials ALTER COLUMN tutorial_plan_id DROP NOT NULL;
COMMENT ON TABLE tutorials
  IS 'A table to store the tutorial sessions and plans of the NuMiTu project.';

CREATE TABLE activities
(
  id bigserial NOT NULL,
  tutorial_id bigint references tutorials(id),
  activity_plan_id bigint references activities(id),
  CONSTRAINT activities_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE activities
  OWNER TO postgres;
COMMENT ON TABLE activities
  IS 'A table to store the root activity data. This data will be common across
  all activities and any activity specific data will be stored in a child table.';
COMMENT ON COLUMN activities.tutorial_id
  IS 'A reference to the tutorial that this activity is a part of. If the
  referenced tutorial is a tutorial plan then this activity is actually the
  default state an activity should be in when the activity begins. If the
  referenced tutorial is a tutorial session than the activity is the state of
  that activity for a given group in a session.';
COMMENT ON COLUMN activities.activity_plan_id
  IS 'A reference to the activity that describes the initial state of this
  activity. If this column is null then this row is an activity plan.';
ALTER TABLE activities ALTER COLUMN activity_plan_id DROP NOT NULL;



CREATE TABLE issue_and_evidence_activities (
    id bigserial NOT NULL,
	activity_id bigint NOT NULL,
	CONSTRAINT issue_and_evidence_activities_pkey PRIMARY KEY (id),
	CONSTRAINT issue_and_evidence_activities_activity_id_fkey FOREIGN KEY (activity_id) REFERENCES activities(id)
);

ALTER TABLE issue_and_evidence_activities OWNER TO postgres;
COMMENT ON TABLE issue_and_evidence_activities IS 'A table to store the issue and evidence activity information';


CREATE TABLE topics (
  id bigserial NOT NULL,
  issues_and_evidences_activity_id bigint NOT NULL,
  name text NOT NULL,
	CONSTRAINT topics_pkey PRIMARY KEY (id),
	CONSTRAINT evidences_issue_id_fkey FOREIGN KEY (issues_and_evidences_activity_id) REFERENCES issue_and_evidence_activities(id)
);


ALTER TABLE topics OWNER TO postgres;
COMMENT ON TABLE topics IS 'A table to store the information about topics relating to a particular issue and evidence activity';

CREATE TABLE issues (
  id bigserial NOT NULL,
  topic_id bigint NOT NULL,
  data text NOT NULL,
	CONSTRAINT issues_pkey PRIMARY KEY (id),
	CONSTRAINT issues_topic_id_fkey FOREIGN KEY (topic_id) REFERENCES topics(id)
);
ALTER TABLE issues OWNER TO postgres;
COMMENT ON TABLE issues IS 'A table to store the issues which are related to topics';

  
CREATE TABLE evidences (
  id bigserial NOT NULL,
  data text NOT NULL,
  issue_id bigint,
  score integer DEFAULT 10 NOT NULL,
	CONSTRAINT evidences_pkey PRIMARY KEY (id),
	CONSTRAINT evidences_issue_id_fkey FOREIGN KEY (issue_id) REFERENCES issues(id)
);


ALTER TABLE evidences OWNER TO postgres;
COMMENT ON TABLE evidences IS 'A table to store the of pieces of evidence for the various issues';
